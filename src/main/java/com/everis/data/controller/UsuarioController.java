package com.everis.data.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UsuarioController {
	
	@RequestMapping("/")
	public String index() {		
		return "index.jsp";
	}
	
	@RequestMapping("/login")
	public String formUsuario(Model model, @RequestParam(value="nombre") String nombre,
			 @RequestParam(value="apellido") String apellido,
			 @RequestParam(value="limite") String limite,
			 @RequestParam(value="codigo") String codigo) {
		
		model.addAttribute("nombre", nombre);
		model.addAttribute("apellido", apellido);
		model.addAttribute("limite", limite);
		model.addAttribute("codigo", codigo);	
		
		if(nombre.isBlank() && apellido.isBlank() && limite.isBlank() && codigo.isBlank()) {
			model.addAttribute("errNombre", "Debe ingresar un nombre");		
			model.addAttribute("errApellido", "Debe ingresar el apellido");
			model.addAttribute("errLimite", "Debe ingresar un limite");
			model.addAttribute("errCodigo", "Debe ingresar un codigo postal");
			return "index.jsp";
		}
		return "index.jsp";
	}
}
