<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Registro de Usuario</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<h1 style="text-align:center">Registro de Usuario</h1>

	<div class ="container">
		<form action="/login", method="post">
        <table class="table table-striped" >           
            <tr>
                <th >Nombre</th>
                <th><input  type="text" class="form-control" name="nombre"><c:out value="${errNombre}"></c:out></th>              
            </tr>                 
            <tr >
                <th >Apellido</th>
                <th><input  type="text" class="form-control"  name="apellido"><c:out value="${errApellido}"></c:out></th>
            </tr>
            <tr>
            	<th >L�mite</th>
            	<th><input  type="text" class="form-control" name="limite"> <c:out value="${errLimite}"></c:out></th>
            </tr>
            <tr>
            	<th >C�digo Postal</th>
            	<th><input  type="text" class="form-control" name="codigo"> <c:out value="${errCodigo}"></c:out></th>
            </tr>
            <tr>
                <th>
                </th>  
                <th><a class="btn btn-warning">Limpiar</a>
                	<input type="submit" class="btn btn-success" value="Submit">
                </th>              	                      
            </tr>
        </table>       
        </form>
        <table class="table table-striped">
        	<tr> 
        		<th>Nombre</th>
        		<th>Apellido</th>
        		<th>Limite</th>
        		<th>Co�digo Postal</th>
        	</tr>
        	<tr> 
        		<th><c:out value="${nombre}"></c:out></th>
        		<th><c:out value="${apellido}"></c:out></th>
        		<th><c:out value="${limite}"></c:out></th>
        		<th><c:out value="${codigo}"></c:out></th>
        	</tr>
        </table>
	</div>

</body>
</html>